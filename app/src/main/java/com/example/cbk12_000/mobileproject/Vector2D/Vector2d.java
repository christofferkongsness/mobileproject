package com.example.cbk12_000.mobileproject.Vector2D;


/*
A class for vector math with double precision
 */
public class Vector2d {
    /**
     * The x component of the vector
     */
    public double x;
    /**
     * The y component of the vector
     */
    public double y;

    /**
     * Creates an empty vector without the components initialized
     */
    public Vector2d()
    {

    }

    /**
     * Creates a new vector
     * @param x The X component of the vector
     * @param y The Y component of the vector
     */
    public Vector2d(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    /**
     * Adds this vector with another
     *
     * @param vec The vector to add
     * @return The added vector
     */
    public Vector2d add(Vector2d vec)
    {
        return new Vector2d(x + vec.x, y + vec.y);
    }

    /**
     * Subtracts a vector from this one
     * @param vec The vector to subtract from this one
     * @return The subtracted vector
     */
    public Vector2d sub(Vector2d vec)
    {
        return new Vector2d(x - vec.x, y - vec.y);
    }

    /**
     * Multiplies this vector with a number
     *
     * @param num The number to multiply with
     * @return The multiplied vector
     */
    public Vector2d multiply(double num)
    {
        return new Vector2d(x * num, y * num);
    }

    /**
     * Multiples this vector with another
     *
     * @param vec The vector to multiply with
     * @return The multiplied vector
     */
    public Vector2d multiply(Vector2d vec)
    {
        return new Vector2d(x * vec.x, y * vec.y);
    }

    /**
     * Calculates the absolute value of the vector.
     * Should use absSqrt if possible.
     *
     * @see #absSqrt()
     * @return The absolute of the vector
     */
    private double abs()
    {
        return Math.sqrt((x * x) + (y * y));
    }

    /**
     * Calculates the squared absolute value. This is a much lighter
     * operation then abs() and should be used instead where possible.
     *
     * @see #abs()
     * @return The absolute squared of the vector
     */
    public double absSqrt()
    {
        return (x * x) + (y * y);
    }

    /**
     * Calculates the normalized vector of this vector
     * @return The normalized vector
     */
    public Vector2d normalized()
    {
        double length = abs();

        if(length != 0.0)
        {
            return new Vector2d(x / length, y / length);
        }
        else
        {
            return new Vector2d(0.0, 0.0);
        }
    }

    /**
     * Creates a string based on the vector in the form "X: x, Y: y"
     *
     * @return The string
     */
    @Override
    public String toString()
    {
        return  "X: " + x + ", Y: " + y;
    }
}

