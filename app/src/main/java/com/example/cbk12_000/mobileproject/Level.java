package com.example.cbk12_000.mobileproject;

import com.example.cbk12_000.mobileproject.Vector2D.Vector2d;

import java.util.ArrayList;

/**
 * Created by Powie on 01.05.2018.
 */

public class Level {
    private ArrayList<ArrayList<Vector2d>> wallPoints;
    private ArrayList<Vector2d> dangerPoints;
    private Vector2d startPoint;
    private Vector2d goalPoint;
    private String name;

    /**
     * Initializes all variables and sets default values for start and goal
     */
    Level()
    {
        wallPoints = new ArrayList<>();
        dangerPoints = new ArrayList<>();
        startPoint = new Vector2d(50.f,50.f);
        goalPoint = new Vector2d(10.f,10.f);
        name = "";

    }

    /**
     * Adds a pair of points to be used as corners for a wall
     * @param points the points forming the corners of the wall.
     */
    public void addWallPoints(ArrayList<Vector2d> points)
    {
        this.wallPoints.add(points);
    }


    /**
     * Adds a point that will become an obstacle
     * @param point the vector that holds the point's coordinates
     */
    public void addDangerPoints(Vector2d point)
    {

        this.dangerPoints.add(point);
    }

    /**
     * Sets the starting position for the level
     * @param point the starting position
     */
    public void setStartPoint(Vector2d point)
    {
        this.startPoint = point;
    }

    /**
     * Sets the goal point for the level
     * @param goalPoint the goal point
     */
    public void setGoalPoint(Vector2d goalPoint)
    {
        this.goalPoint = goalPoint;
    }

    /**
     * Sets the level name
     * @param name the name of the level
     */
    public void setName(String name)
    {
        this.name = name;
    }


    /**
     * Gets the starting point
     * @return the start point
     */
    public Vector2d getStartPoint()
    {

        return startPoint;
    }

    /**
     * Gets the goal point
     * @return the goal point
     */
    public Vector2d getGoalPoint() {
        return goalPoint;
    }

    /**
     * Gets the points making up the walls
     * @return the wall points
     */
    public ArrayList<ArrayList<Vector2d>> getWallPoints()
    {
        return wallPoints;
    }

    /**
     * Gets the danger points
     * @return the points that act as obstacles
     */
    public ArrayList<Vector2d> getDangerPoints() {
        return dangerPoints;
    }

    /**
     * Gets the level name
     * @return the level name
     */
    public String getName()
    {
        return name;
    }

}
