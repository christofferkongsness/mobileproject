package com.example.cbk12_000.mobileproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class LevelSelection extends AppCompatActivity {

    private String[] levels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_selection);
        final boolean start = getIntent().getBooleanExtra("StartOrHighscore", false);
        ListView listView = findViewById(R.id.listView);


        levels = LevelList.getInstance().getNames();


        listView.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return levels.length;
            }

            @Override
            public Object getItem(int i) {
                return levels[i];
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                // inflate the layout for each list row
                if (view == null) {
                    view = LayoutInflater.from(getBaseContext()).
                            inflate(R.layout.listview, viewGroup, false);
                }

                TextView textViewItemName = view.findViewById(R.id.textview_name);

                textViewItemName.setText(levels[i]);
                return view;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            boolean isStart = start;

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if(isStart) {
                    Intent intent = new Intent(getBaseContext(), GameActivity.class);
                    intent.putExtra("level",i);
                    startActivity(intent);
                }
                else {
                    StartHighScore(i);

                }
            }
        });
    }
    private void StartHighScore(int level)
    {
        Intent intent = new Intent(this, HighScore.class);
        intent.putExtra("level", levels[level]);
        startActivity(intent);
    }

}
